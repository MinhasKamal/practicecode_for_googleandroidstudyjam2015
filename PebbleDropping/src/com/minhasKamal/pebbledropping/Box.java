package com.minhasKamal.pebbledropping;

public class Box {

	int a, b, c, d, e; // i have made these global to use them in any function
	String[][] move;

	public Box() {
		a = b = c = d = e = 5;

		move = new String[6][5];
		int i, j;
		for (i = 0; i < 6; i++)
			// assigning values in the box
			for (j = 0; j < 5; j++)
				move[i][j] = "";
	}

	int assign(int s1, int pl) // this function inputs the value in 'move'
	{
		String symbol;

		if (pl == 1)
			symbol = "O";
		else
			symbol = "X";

		if ((s1 == 1 && a < 0) || (s1 == 2 && b < 0) || (s1 == 3 && c < 0)
				|| (s1 == 4 && d < 0) || (s1 == 5 && e < 0)){
			return 0; // will make know if move is invalid
		}
		else {
			if (s1 == 1) {
				move[a][s1 - 1] = symbol;
				a--;
				return a+2;
			}else if (s1 == 2) {
				move[b][s1 - 1] = symbol;
				b--;
				return b+2;
			}else if (s1 == 3) {
				move[c][s1 - 1] = symbol;
				c--;
				return c+2;
			}else if (s1 == 4) {
				move[d][s1 - 1] = symbol;
				d--;
				return d+2;
			}else {
				move[e][s1 - 1] = symbol;
				e--;
				return e+2;
			}

		}
	}

	int varification(int pl) // finds out the winner
	{
		int v1, v2;
		String symbol;

		if (pl == 1)
			symbol = "O";
		else
			symbol = "X";

		for (v1 = 0; v1 < 6; v1++)
			// finds row match
			for (v2 = 0; v2 < 2; v2++)
				if (move[v1][v2] == symbol && move[v1][v2 + 1] == symbol
						&& move[v1][v2 + 2] == symbol
						&& move[v1][v2 + 3] == symbol)
					return 1;

		for (v1 = 5; v1 > 2; v1--)
			// finds column match
			for (v2 = 0; v2 < 5; v2++)
				if (move[v1][v2] == symbol && move[v1 - 1][v2] == symbol
						&& move[v1 - 2][v2] == symbol
						&& move[v1 - 3][v2] == symbol)
					return 1;

		for (v1 = 3; v1 < 6; v1++)
			// finds right-angle match
			for (v2 = 0; v2 < 2; v2++)
				if (move[v1][v2] == symbol && move[v1 - 1][v2 + 1] == symbol
						&& move[v1 - 2][v2 + 2] == symbol
						&& move[v1 - 3][v2 + 3] == symbol)
					return 1;

		for (v1 = 3; v1 < 6; v1++)
			// finds left-angles match
			for (v2 = 3; v2 < 5; v2++)
				if (move[v1][v2] == symbol && move[v1 - 1][v2 - 1] == symbol
						&& move[v1 - 2][v2 - 2] == symbol
						&& move[v1 - 3][v2 - 3] == symbol)
					return 1;

		return 0;
	}
}
