package com.minhasKamal.pebbledropping;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {

	Box box;
	int player;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		box = new Box();
		player = 1;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void buttonClicked(View view){
		int color;
		if(player==1){
			color = Color.GREEN;
		}else{
			color = Color.RED;
		}

		Button button = (Button)findViewById(view.getId());
		if(button.getId()==R.id.col1){
			int row = box.assign(1, player);
			
			if(row==0){
				return;
			}
			if(row==1){
				findViewById(R.id.col1row1).setBackgroundColor(color);
			}else if(row==2){
				findViewById(R.id.col1row2).setBackgroundColor(color);
			}else if(row==3){
				findViewById(R.id.col1row3).setBackgroundColor(color);
			}else if(row==4){
				findViewById(R.id.col1row4).setBackgroundColor(color);
			}else if(row==5){
				findViewById(R.id.col1row5).setBackgroundColor(color);
			}else if(row==6){
				findViewById(R.id.col1row6).setBackgroundColor(color);
			}
			
			
			
		}else if(button.getId()==R.id.col2){
			int row = box.assign(2, player);
			
			if(row==0){
				return;
			}
			if(row==1){
				findViewById(R.id.col2row1).setBackgroundColor(color);
			}else if(row==2){
				findViewById(R.id.col2row2).setBackgroundColor(color);
			}else if(row==3){
				findViewById(R.id.col2row3).setBackgroundColor(color);
			}else if(row==4){
				findViewById(R.id.col2row4).setBackgroundColor(color);
			}else if(row==5){
				findViewById(R.id.col2row5).setBackgroundColor(color);
			}else if(row==6){
				findViewById(R.id.col2row6).setBackgroundColor(color);
			}
			
			
		}else if(button.getId()==R.id.col3){
			int row = box.assign(3, player);
			
			if(row==0){
				return;
			}
			if(row==1){
				findViewById(R.id.col3row1).setBackgroundColor(color);
			}else if(row==2){
				findViewById(R.id.col3row2).setBackgroundColor(color);
			}else if(row==3){
				findViewById(R.id.col3row3).setBackgroundColor(color);
			}else if(row==4){
				findViewById(R.id.col3row4).setBackgroundColor(color);
			}else if(row==5){
				findViewById(R.id.col3row5).setBackgroundColor(color);
			}else if(row==6){
				findViewById(R.id.col3row6).setBackgroundColor(color);
			}
			
			
		}else if(button.getId()==R.id.col4){
			int row = box.assign(4, player);
			
			if(row==0){
				return;
			}
			if(row==1){
				findViewById(R.id.col4row1).setBackgroundColor(color);
			}else if(row==2){
				findViewById(R.id.col4row2).setBackgroundColor(color);
			}else if(row==3){
				findViewById(R.id.col4row3).setBackgroundColor(color);
			}else if(row==4){
				findViewById(R.id.col4row4).setBackgroundColor(color);
			}else if(row==5){
				findViewById(R.id.col4row5).setBackgroundColor(color);
			}else if(row==6){
				findViewById(R.id.col4row6).setBackgroundColor(color);
			}
			
			
		}else if(button.getId()==R.id.col5){
			int row = box.assign(5, player);
			
			if(row==0){
				return;
			}
			if(row==1){
				findViewById(R.id.col5row1).setBackgroundColor(color);
			}else if(row==2){
				findViewById(R.id.col5row2).setBackgroundColor(color);
			}else if(row==3){
				findViewById(R.id.col5row3).setBackgroundColor(color);
			}else if(row==4){
				findViewById(R.id.col5row4).setBackgroundColor(color);
			}else if(row==5){
				findViewById(R.id.col5row5).setBackgroundColor(color);
			}else if(row==6){
				findViewById(R.id.col5row6).setBackgroundColor(color);
			}
			
			
		}
		
		int result = box.varification(player);
		if(result==1){
			TextView textView = (TextView) findViewById(R.id.text);
			textView.setText("Player-"+player+" wins!");
			
			findViewById(R.id.col1).setEnabled(false);
			findViewById(R.id.col2).setEnabled(false);
			findViewById(R.id.col3).setEnabled(false);
			findViewById(R.id.col4).setEnabled(false);
			findViewById(R.id.col5).setEnabled(false);

		}
		
		player = 3-player;
	}
}
