package com.minhaskamal.courtcounter;

import com.minhaskamal.courtcounter.R;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends ActionBarActivity {
	
	private int teamAPoints;
	private int teamBPoints;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		teamAPoints=0;
		displayForTeamA(teamAPoints);
		teamBPoints=0;
		displayForTeamB(teamBPoints);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private void displayForTeamA(int points) {
		TextView scoreView = (TextView) findViewById(R.id.team_a_score);
		scoreView.setText(points+"");
	}
	
	private void displayForTeamB(int points) {
		TextView scoreView = (TextView) findViewById(R.id.team_b_score);
		scoreView.setText(points+"");
	}
	
	public void addThreeForTeamA(View view){
		teamAPoints += 3;
		displayForTeamA(teamAPoints);
	}
	
	public void addTwoForTeamA(View view){
		teamAPoints += 2;
		displayForTeamA(teamAPoints);
	}

	public void freeThrowForTeamA(View view){
		teamAPoints += 1;
		displayForTeamA(teamAPoints);
	}
	
	public void addThreeForTeamB(View view){
		teamBPoints += 3;
		displayForTeamB(teamBPoints);
	}
	
	public void addTwoForTeamB(View view){
		teamBPoints += 2;
		displayForTeamB(teamBPoints);
	}

	public void freeThrowForTeamB(View view){
		teamBPoints += 1;
		displayForTeamB(teamBPoints);
	}
	
	
	public void reset(View view){
		teamAPoints = 0;
		displayForTeamA(teamAPoints);
		
		teamBPoints = 0;
		displayForTeamB(teamBPoints);
	}
}
